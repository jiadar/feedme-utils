defmodule Scorecard.Calc do

  def calc_score(item) do

    %{dish_name: name, carbohydrates: c, total_fat: f, protein: p, sat_fat: fs, fiber: r, sugar: s,
      sodium: n, calories: kg, cholesterol: h, dish_id: id} = item
    
    # TODO: These constants should be in a database
    # TODO: allowt these constants to be adjusted by the user (? no requirements yet)
    const_p = 500
    const_d = 300
    const_fm = 20
    const_cm = 50
    const_pm = 30

    k = max(1, 9 * f + 4 * (c + p))     # Calculate calories, must not be zero
    c = max(1, c)                       # Must not be zero
    f = max(1, f)                       # Must not be zero 
    kg = max(1, kg)                     # Must not be zero
    er = kg - k                         # Calculate error

    # Include Filter - check if it makes sense to compute the score

    f1 = (c - s) > 0
    f2 = (f - fs) > 0
    f3 = (er < 100)
    f4 = ((100 * er) / kg) < 17
    f5 = kg > 100

    # Ross added this filter to deal with the -1 default values in the database
   
    f6 = ( (c >= 0) and (f >= 0) and (fs >= 0) and (r >= 0) and (s >= 0) and (n >= 0) or (kg >= 0) or (h >=0 ))
    
    if (f1 and f2 and f3 and f4 and f5 and f6) do

      fp = (900 * f) / k
      cp = (400 * c) / k
      pp = (400 * p) / k
      m = abs(fp - const_fm) + abs(cp - const_cm) + abs(pp - const_pm)
      pp = m / const_p

      g = (1 / k) * (4 * r + pp) + (6 * :math.pow(r, 1.3) / c)
      g = g + (3 * (f - fs) / (10 * f))
      g = g + ((c - r - s) / (10 * c))
      g = g + (4 * :math.pow(r, 1.2) / 28)
      g = g + (1.1 * :math.pow(p, 1.3) / 60)

      b = (8 * :math.pow(max(0,fs), 1.4) / f)
      b = b + s / c
      
      # intermediate result to be multiplied by 1/k 
      b_int = (4 * :math.pow(f, 1.4))
      b_int = b_int + 12 * :math.pow(9 * f, 1.3)
      b_int = b_int + 3 * fs
      b_int = b_int + 3 * :math.pow(n, 1.015)
      b_int = b_int + 3 * h
      b_int = b_int + 2 * c
      b_int = b_int + 4 * s

      # TODO: Extract out these constants and potentially make them user definable 
      b = b + (1 / k) * b_int 
      b = b + 3 * k / 2000
      b = b + 3 * f / 60
      b = b + 3 * fs / 20
      b = b + n / 2400
      b = b + 4 * h / 300
      b = b + 4 * c / 300
      b = b + 4 * s / 50      

      d = (1 - pp) * (const_d * g / b)
      
      e = max(d, 0)
      e = min(e, 100)

      %{score: round(e), filtered: false, name: name, id: id}
      
    else # did not pass filter
      %{filtered: true, name: name, score: 0, id: id}
    end          
  end
end

