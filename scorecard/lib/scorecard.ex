defmodule Scorecard do
  import Scorecard.MenuItem
  import Ecto.Query
  import Scorecard.Calc
  alias Scorecard.{MenuItem, Repo}

  # Compute the scores in parallel by sharding on first 4 bits of UUID.
  # TODO: use the args to do something useful (specificy file or direct import, data file name, limit, etc)
  def main(_args \\ []) do
    ?0..?9 |> parallel_compute
    ?a..?f |> parallel_compute
  end

  # For the range given, compute a shard of results in a separate process
  defp parallel_compute(range) do
    me = self()
    range
    |> Enum.map(fn (e) -> spawn_link fn -> (send me, { self(), compute_shard(e)}) end end)
    |> Enum.map(fn (pid) -> receive do { ^pid, result } -> result end end)
  end

  # Given the first 4 bits defining the shard as a char representing a hex number, write the results to a file
  defp compute_shard(letter) do
    letter
    |> uuid_range
    |> result_set 
    |> List.to_string
    |> write_to_file
  end

  # Given the first 4 bits defining the shard, produce a UUID range representing the shard 
  defp uuid_range(letter) do
    letter = List.to_string([letter])
    shard_start = UUID.string_to_binary!(letter <> "0000000-0000-0000-0000-000000000000")
    shard_end = UUID.string_to_binary!(letter <> "fffffff-ffff-ffff-ffff-ffffffffffff")
    %{shard_start: shard_start, shard_end: shard_end}
  end

  # Given a string, write it to the file
  # TODO: Use Ecto to directly insert values into the DB 
  # TODO: file should be an argument and not statically coded
  defp write_to_file(result) do
    {:ok, file} = File.open "data.txt", [:append]
    IO.binwrite file, result
    File.close file
  end

  # Compute a result set given a shard start and end by obtaining data, calculating score, and dropping
  # filtered items
  defp result_set(%{shard_start: shard_start, shard_end: shard_end}) do
    MenuItem
    |> limit(300000)
    |> where([m], like(m.allergens, "%C%"))
    |> where([m], m.dish_id >= ^shard_start)
    |> where([m], m.dish_id <= ^shard_end)
    |> Repo.all()
    |> Enum.map(&calc_score/1)
    |> Enum.reduce([], &drop_filtered/2)
  end

  # Drop items if filtered is set to true (item fails filter in calc)
  defp drop_filtered(score, acc) do
    %{filtered: filtered, score: score, id: id} = score
    if (filtered) do
      acc
    else
      sql = "update menu_data set scorecard=" <> Integer.to_string(score) <> " "
      sql = sql <> "where dish_id='" <> UUID.binary_to_string!(id) <> "';\n"
      acc ++ [ sql ]
    end 
  end

end

