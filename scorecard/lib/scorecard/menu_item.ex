defmodule Scorecard.MenuItem do
  use Ecto.Schema
  import Ecto
  import Ecto.Changeset
  import Ecto.Query, only: [from: 2]

  @primary_key {:dish_id, :binary, []}
  
  schema "menu_data" do
    field :restaurant_id, :binary
    field :dish_name, :string
    field :dish_description, :string
    field :serving_size_grams, :integer
    field :serving_per_unit, :integer
    field :calories, :float
    field :total_fat, :float
    field :sat_fat, :float
    field :mono_fat, :float
    field :trans_fat, :float
    field :cholesterol, :float
    field :sodium, :float
    field :potassium, :float
    field :carbohydrates, :float
    field :fiber, :float
    field :sugar, :float
    field :sugar_alcohol, :float
    field :protein, :float
    field :allergens, :string
    field :ingredients, :string
    field :weights, :string
    field :tags, :string
    field :ingredients_removables, :string
    field :addons, :string
    field :image_link, :string
    field :price, :float
    field :vitamin_a, :float
    field :vitamin_c, :float
    field :calcium, :float
    field :iron, :float
    field :caffeine, :float
    field :alcohol, :float
    field :health_score_s, :integer
    field :health_score_m, :integer
    field :health_score_l, :integer
    field :health_score_s_pro, :integer
    field :health_score_m_pro, :integer
    field :health_score_l_pro, :integer
    field :health_score_s_sug, :integer
    field :health_score_m_sug, :integer
    field :health_score_l_sug, :integer
    field :health_score_s_heart, :integer
    field :health_score_m_heart, :integer
    field :health_score_l_heart, :integer
    field :flavor_profile, :string
    field :rating, :float
    field :num_rating, :integer
  end

end
