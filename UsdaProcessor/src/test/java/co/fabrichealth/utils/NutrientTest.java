package co.fabrichealth.utils;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/* Test Nutrient Class */
public class NutrientTest  {
 
	@Test
	public void CreateNewNutrient() {
		Nutrient n = new Nutrient();
		assertEquals(n.name(), "");
	}
 
	@Test
	public void NutrientShouldParse1() {
		Nutrient n = new Nutrient("\"Carbohydrate, by difference\",g,--,--,28.00,9.86");
		assertEquals(n.name(), "Carbohydrate, by difference");
		assertEquals(n.unit(), "g");
		assertEquals(n.amount(), 9.86, 0.01);
		assertEquals(n.category(), Nutrient.Category.UNCATEGORIZED);
	}

	public void NutrientShouldParse2() {
		Nutrient n = new Nutrient("\"Cholesterol\",mg,--,--,11,4", Nutrient.Category.LIPID);
		assertEquals(n.name(), "Cholesterol");
		assertEquals(n.unit(), "mg");
		assertEquals(n.amount(), 4.0, 0.01);
		assertEquals(n.category(), Nutrient.Category.LIPID);
	}
	
}

