package co.fabrichealth.utils.generators;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Optional;
import java.util.ArrayList;
import java.util.logging.Logger;
import co.fabrichealth.utils.Ingredient;
import co.fabrichealth.utils.Nutrient;

/**
 * A generator to produce CSV output. 
 *
 * This is called by the UsdaProcessor when attempting to generate a file of this type.  
 *
 * To make a new generator, you may want to extend this class instead of writing one from scratch. 
 */
public class CsvGenerator implements UsdaGenerator {

	private final static Logger LOGGER = Logger.getLogger(CsvGenerator.class.getName());
	
	String usdaDirectory;                       // The directory the USDA files are in
	int success;                                // The number of entries we successfully parsed
	int total;                                  // The total number entries we attempted to parse
	boolean hasRun;                             // Have we run the generator at least once? 
	ArrayList<String> nutrientsToImport;        // Of the 172 nutrients, which ones are we importing

	// TODO: change default file to a generic file in the local directory
	/** Create a new CsvGenerator with a default file */
	public CsvGenerator() {
		this("/Users/ross/work/fabric-data/USDA_FULL/");
	}

	/** 
	 * Create a new CsvGenerator with a supplied file directory, adding a default list of 
	 * nutrients to import. 
	 *
	 * NOTE: Can not use regex qualifers in these strings, as internally a regex is used to do the
	 * matching. For instance, Total lipid (fat) will not match while Total lipid will match. Beware. 
	 *
	 * @param dir Directory containing USDA_FULL
	 */
	public CsvGenerator(String dir) {
		usdaDirectory = dir;
		success = 0;
		total = 0;
		hasRun = false;
		nutrientsToImport = new ArrayList<String>();

		// This was made with the following sed excerpt:
		// sed -e 's/\(.*\)/nutrientsToImport\.add\("\1\"\);/g'
		nutrientsToImport.add("Energy");
		nutrientsToImport.add("Alcohol, ethyl");
		nutrientsToImport.add("Caffeine");
		nutrientsToImport.add("Calcium, Ca");
		nutrientsToImport.add("Carbohydrate, by difference");
		nutrientsToImport.add("Cholesterol");
		nutrientsToImport.add("Fatty acids, total monounsaturated");
		nutrientsToImport.add("Fatty acids, total saturated");
		nutrientsToImport.add("Fatty acids, total trans");
		nutrientsToImport.add("Fiber, total dietary");
		nutrientsToImport.add("Iron, Fe");
		nutrientsToImport.add("Potassium, K");
		nutrientsToImport.add("Protein");
		nutrientsToImport.add("Sodium, Na");
		nutrientsToImport.add("Sugars, total");
		nutrientsToImport.add("Total lipid");                        // Can not use (fat) in match string
		nutrientsToImport.add("Vitamin A, IU");
		nutrientsToImport.add("Vitamin C, total ascorbic acid");
		nutrientsToImport.add("Vitamin D");		
	}
	
	/** 
	 * Return the current list of nutrients we will import upon run
	 *
	 * @return nutrientsToImport A list of strings representing text search of nutrients
	 */
	public ArrayList<String> getNutrientsToImport() {
		return nutrientsToImport;
	}

	/** 
	 * Set the nutrients to import as a list. 
	 *
	 * @param list A list of strings representing text search of nutrients
	 */
	public void setNutrientsToImport(ArrayList<String> list) {
		nutrientsToImport = list; 
	}
	
	/** 
	 * Generates suitable output to the file string returned. 
	 *
	 * @return filename The name of the file containing the generated data
	 */
	public String run() {
		return run(2147483647);
	} 

	/** 
	 * Generates suitable output to the file string returned up to max entries. 
	 *
	 * @param max the maximum number of entries to process
	 * @return filename The name of the file containing the generated data
	 */
	public String run(int max) {
		String rval = "";
		Ingredient i = new Ingredient();
		success = 0;
		total = 0;

		LOGGER.info("Generator starting (will generate up to " + max + " entries)");
		
		try {
			BufferedWriter outputFile = getBufferedWriter("generated.out");    // Open file for output

			// Loop through all files in the USDA directory, up to max. Attempt to add the new
			// new ingredient. If we can't detect the type, we will fail. If we detect the type,
			// attempt to parse it with the parser. Finally, write it to the csv file. 
			
			final File file = new File(usdaDirectory);
			for (File f : file.listFiles()) {                            // Loop through the files
				total++;
				i = new Ingredient(f);
				String n = i.name();

				if (total == max) {                                       // Break if we have reached max
					break;
				}

				if (n.length() > 0) {                                     // Fail if name doesn't exist
					Optional<Nutrient> energy = i.findNutrient("Energy");
					if (energy.isPresent()) {                              // Succeed if a nutrient exists
						success++;
						outputFile.write(generateOutput(i));                // Write it to the file
					}
				}

				else {                                    
					LOGGER.info("Failed: " + f.getName());                 // Log failed entries
				}
				
				if (total % 1000 == 0) {
					LOGGER.info("Processed " + total);                          // Log progress every 1000 entries
				}
			}
			outputFile.close();
		}

		// Catch the exceptions we will throw generating parse errors on known format types
		// We would want to fix these errors in the parser. We will fail silently for unknown types. 

		catch(NumberFormatException | IOException | NullPointerException e) {
			LOGGER.info("Parse Error");
		}

		hasRun = true;
		return usdaDirectory;
	}

	/** 
	 * Creates an output file handle 
	 *
	 * @param fn Filename for output 
	 * @return out_bw Buffered writer containing file handle 
	 * @throws IOException if file can not be opened 
	 */
	protected BufferedWriter getBufferedWriter(String fn) throws IOException {
		File outputFile = new File(fn);
		outputFile.createNewFile();
		FileWriter out_fw = new FileWriter(outputFile, true);
		BufferedWriter out_bw = new BufferedWriter(out_fw);
		return out_bw;
	}
	
	/** 
	 * Generates CSV output for one ingredient. Override this method if you extend this class. 
	 * 
	 * Using the contents of the ingredient, create a string in the desired format. 
	 *
	 * @param i ingredient to generate output for
	 * @return rval The string in the desired format
	 */
	protected String generateOutput(Ingredient i) {
		String rval = "";
		rval += qq(i.name()) + c() + qq(i.ndb()) + c() + qq(i.upc()) + c();
		for (String nutrientStr : nutrientsToImport) {
			Optional<Nutrient> n = i.findNutrient(nutrientStr);
			rval += ((n.isPresent()) ? n.get().amount() : 0) + c();            // Print 0 if nutrient is not present
		}
		rval = rval.substring(0, rval.length()-1) + "\n";                     // Chop last comma, add newline
		return rval;
	}
	
	/** 
	 * Quotes a string intelligently. Override this method if you extend this class and need to modify
	 * the quoting method or escapes. 
	 *
	 * @param  s String to be quoted or escaped
	 * @return rval String properly quoted or escaped for desired format
	 */
	protected String qq(final String s) {
		String rval = new String();
		if (s.length() == 0) {
			return " ";
		}		
		rval = "\"" + rval + "\"";              // postgres style quote the string
		return rval;
	}

	/** 
	 * Convenience method to return a comma 
	 *
	 * @return comma String of a comma
	 */
	protected String c() {
		return ",";
	}
		
	/** 
	 * Determines if the generator has run (at least once) or not.
	 *
	 * @return boolean true if the generator has run, false if not
	 */ 
	public boolean hasRun() {
		return hasRun;
	}
	
	/** 
	 * Returns the number of successes detected in parsing. 
	 * If generator has not run, will be 0. 
	 *
	 * @return numSuccesses the number of successful entries generated
	 */
	public int getSuccesses(){
		return success;
	}

	/** 
	 * Returns the number of failures detected in parsing 
	 * If generator has not run, will be 0. 
	 *
	 * @return numFailures the number of unsuccessful entries discarded
	 */
	public int getFailures() {
		return total - success;
	}

	/** 
	 * Returns the total number of entries attempted to generate
	 * If generator has not run, will be 0. 
	 *
	 * @return total number of total entries attempted to generate
	 */
	public int getTotal() {
		return total;
	}

	/** 
	 * Returns the generated yield as a double less than one
	 * If generator has not run, will be 0. 
	 *
	 * @return yield successes divided by total
	 */
	public double yield() {
		return success * 1.0 / total;
	}
}
