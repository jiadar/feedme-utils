package co.fabrichealth.utils.generators;

import java.util.Optional;
import java.util.logging.Logger;
import java.util.ArrayList;
import co.fabrichealth.utils.Ingredient;
import co.fabrichealth.utils.Nutrient;
import co.fabrichealth.utils.generators.CsvGenerator;

/**
 * A generator interface to output nutrition data by implementing generators. 
 *
 * This is called by the UsdaProcessor when attempting to generate a file of this type.  
 *
 * To make a new generator, implement this class. 
 */
public class PostgresGenerator extends CsvGenerator implements UsdaGenerator {

	private final static Logger LOGGER = Logger.getLogger(PostgresGenerator.class.getName());

	// TODO: change default file to a generic file in the local directory
	/** Create a new PostgresGenerator with a default file */
	public PostgresGenerator() {
		this("/Users/ross/work/fabric-data/USDA_FULL/");
	}
   
	/** 
	 * Create a new PostgresGenerator with a supplied file directory, adding a default list of 
	 * nutrients to import. 
	 *
	 * @param dir Directory containing USDA_FULL
	 */
	public PostgresGenerator(String dir) {
		super();
	}

	/** 
	 * Generates Postgres output for one ingredient 
	 *
	 * @param i ingredient to generate output for
	 */
	protected String generateOutput(Ingredient i) {
		String rval = "insert into ingredients ";
		rval += "(name, filename, category, ndb, upc, energy, alcohol, caffene, calcium, carbohydrate, cholesterol, monofat, ";
      rval += "satfat, transfat, ";
		rval += "fiber, iron, potassium, protein, sodium, sugars, fat, vitamin_a, vitamin_c, vitamin_d, ";
      rval += "txt_category, txt_prep, txt_upc, txt_brand, txt_branded, txt_organic, txt_cook_method, txt_storage, ";
      rval += "txt_modified, txt_matter_type, txt_liquid_type, txt_pyramid, txt_color, txt_size";
      rval += ") values (";	
		rval += qq(i.name()) + c() + qq(i.fileName()) + c() + qq(i.group()) + c() + qq(i.ndb()) + c() + qq(i.upc()) + c();

      // insert nutrients
      for (String nutrientStr : nutrientsToImport) {
			rval += i.nutrientSum(nutrientStr) + c();
		}

      // insert search and filter keys
      rval += qq(categoryText(i)) + c() + qq(prepText(i)) + c() + qq(upcText(i)) + c() + qq(brandText(i)) + c();
      rval += qq(brandedText(i)) + c() +  qq(organicText(i)) + c() + qq(cookingMethodText(i)) + c() + qq(storageText(i));
      rval += c() + qq(modifiedText(i)) + c() + qq(matterTypeText(i)) + c() ;
      rval += qq(liquidTypeText(i)) + c() + qq(pyramidTypeText(i)) + c() + qq(colorText(i)) + c();
      rval += qq(sizeText(i)) + c();
      
		rval = rval.substring(0, rval.length()-1) + ");\n";                     // Chop last comma, close sql stmt
		return rval;
	}

	/** 
	 * Quotes a string intelligently. Postgres requires specific quoting semantics implemented here.
	 *
	 * @param  s String to be quoted or escaped
	 * @return rval String properly quoted or escaped for desired format
	 */
	protected String qq(final String s) {
		String rval = new String();
		if (s.length() == 0) {
			return "''";
		}		
		rval = s.replace("'","''");             // If there is a ' in the string, it needs to be '' in order to parse by psql
		rval = "\'" + rval + "\'";              // postgres style quote the string
		return rval;
	}

   // TODO: Do this a better way
   
   private String categoryText(Ingredient i) {
      return i.group();
   }

   private boolean testName(Ingredient i, String s) {
      return (i.name().toLowerCase().contains(s.toLowerCase()));
   }
   
   private String prepText(Ingredient i) {
      if (i.name().toLowerCase().contains("raw")) return "raw";
      if (i.name().toLowerCase().contains("cooked")) return "cooked";
      return "";
   }

   private String upcText(Ingredient i) {
      if (i.upc().length() > 1) return "UPC Present";
      return "";
   }

   private String brandText(Ingredient i) {
      //TODO: Detection method for brands
      return "";
   }
   
   private String brandedText(Ingredient i) {
      if (i.group().toLowerCase().contains("branded")) return "branded";
      return "";
   }

   private String organicText(Ingredient i) {
      if (testName(i, "organic")) return "organic";
      return "";
   }

   private String cookingMethodText(Ingredient i) {
      if (testName(i, "fried")) return "fried";
      if (testName(i, "boiled")) return "boiled";
      if (testName(i, "cooked")) return "cooked";
      if (testName(i, "toasted")) return "toasted";
      if (testName(i, "dry")) return "dry";
      if (testName(i, "raw")) return "raw";
      return "";
   }

   private String storageText(Ingredient i) {
      if (testName(i, "fresh")) return "fresh";
      if (testName(i, "frozen")) return "frozen";
      return "";
   }

   private String modifiedText(Ingredient i) {
      if (testName(i, "non fat")) return "non fat";                               
      if (testName(i, "low fat")) return "low fat";
      if (testName(i, "high fat")) return "high fat";
      if (testName(i, "low sodium")) return "low sodium";
      return "";
   }

   private String matterTypeText(Ingredient i) {
      if (testName(i, "liquid")) return "liquid";
      if (testName(i, "solid")) return "solid";
      return "";
   }

   private String liquidTypeText(Ingredient i) {
      if (testName(i, "sauce")) return "sauce";
      if (testName(i, "juice")) return "juice";
      if (testName(i, "gravy")) return "gravy";
      if (testName(i, "dip")) return "dip";
      if (testName(i, "beverage")) return "beverage";
      if (testName(i, "drink")) return "drink";
      if (testName(i, "soup")) return "soup";
      if (testName(i, "coffee")) return "coffee";
      if (testName(i, "tea")) return "tea";
      return "";
   }

   private String pyramidTypeText(Ingredient i) {
      // TODO: Need to determine a search for this 
      return "";
   }
   
   private String colorText(Ingredient i) {
      if (testName(i, "blue")) return "blue";
      if (testName(i, "black")) return "black";
      if (testName(i, "grey")) return "grey";
      if (testName(i, "red")) return "red";
      if (testName(i, "orange")) return "orange";
      if (testName(i, "purple")) return "purple";
      if (testName(i, "magenta")) return "magenta";
      if (testName(i, "green")) return "green";
      return "";
   }

   private String sizeText(Ingredient i) {
      if (testName(i, "tiny")) return "tiny";
      if (testName(i, "small")) return "small";
      if (testName(i, "medium")) return "medium";
      if (testName(i, "large")) return "large";
      if (testName(i, "extra large")) return "extra large";
      if (testName(i, "huge")) return "huge";
      return "";
   }
   
}
