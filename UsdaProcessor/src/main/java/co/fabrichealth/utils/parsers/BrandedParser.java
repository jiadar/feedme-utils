package co.fabrichealth.utils.parsers;

import java.util.Date;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import co.fabrichealth.utils.Nutrient;

/**
 * A parser to extract nutrition data from branded items of the USDA database.
 *
 * This is called by the Ingredient when attempting to parse a file detected as this type. 
 * Then the getter methods of this parser ae used to set the ingredient instance values. 
 *
 * NOTE: Do not copy this class to make a new parser, copy the StdParser class and modify it. 
 *
 */
public class BrandedParser implements UsdaParser {

	private static final Pattern NUTRIENT_REGEX =
		Pattern.compile("Proximates|Minerals|Vitamins|Lipids|Amino Acids|Other");

	private static final Pattern LINE3_REGEX = 
		Pattern.compile("\"Nutrient data for: (\\d+), (.*) (UPC|GTIN): (\\d+)");
	
	private String source;
	private Date collection_date;
	private String ndb;
	private String upc;
	private String name;
	private String group;
	private String common_name;
	private List<Nutrient> nutrients;
	private String ingredients;

	public BrandedParser() {
		source = "unknown";
		collection_date = new Date();
		ndb = "";
		upc = "";
		name = "";
		group = "";
		common_name = "";
		nutrients = new ArrayList<Nutrient>();
		ingredients = "";
	}

	public BrandedParser(final List<String> data) {
		this();
		parseHeaders(data);
		parseNutrients(data);
		parseIngredients(data);
	}
	
	public String source() {
		return source;
	}

	public Date collection_date() {
		return collection_date;
	}

	public String ndb() {
		return ndb;
	}

	public String upc() {
		return upc;
	}

	public String name() {
		return name;
	}

	public String group() {
		return group;
	}

	public String cn() {
		return common_name;
	}

	public List<Nutrient> nutrients() {
		return nutrients;
	}

	public String ingredients() {
		return ingredients;
	}
	
	/**
	 * Parses nutrients from a list of strings as formatted in the scrape
	 * data file. Populates the nutrients in the ingredient instance. 
	 *
	 * The scraped data file contains nutrient headers and nutrients below those headers
	 * starting on line 7 and continuing through the end of the file, minus the last 2
	 * lines which are a string of ingredients.
	 *
	 * @param data  List of strings containing data from the scraped file
	 * @returns Populated instance variables for nutrients
	 */	
	private void parseNutrients(final List<String> data) {

		// Create an iterator with the subset of the list that contains nutrient values

		Iterator<String> dataIterator = data.subList(7, data.size() - 2).iterator();

		// Loop through the iterator adding nutrients that don't match the regex defining
		// nutrient types. When we find a line that doesn't match this, add a new
		// nutrient to the list with the constructor, which parses the CSV string

		while (dataIterator.hasNext()) {
			String line = dataIterator.next();
			Matcher m = NUTRIENT_REGEX.matcher(line);
			if (!m.find()) {
				nutrients.add(new Nutrient(line));
			}
		}
	}

	/** 
	 * Parses the headers from a list of strings as formated in the scrape
	 *
	 * The scraped data contains headers in the first few lines. Here we extract those headers 
	 * into the instance variables. 
	 *
	 * @param data  List of strings containing the data from the scraped file
	 * @return  Populated instance variables for headers
	 */
	private void parseHeaders(final List<String> data) {
		source = data.get(0).substring(8);

		Matcher m = LINE3_REGEX.matcher(data.get(2));
		if (m.find()) {
			ndb = m.group(1);
			name = m.group(2).replace(",","");
			upc = m.group(4);
		}
		group = data.get(3).replace("Food Group:  ", "");
		common_name = data.get(4).replace("Common Name:", "");
	}

	/**
	 * Parses the ingredients from a list of strings formatted in the scrape
	 *
	 * The scraped data has the last line containing ingredients. Here we extract the ingredients
	 * into the instance variables
	 *
	 * @param data  List of strings containing the data from the scraped file
	 * @return  Populated instance variables for ingredients 
	 */
	private void parseIngredients(final List<String> data) {
		String b = data.get(data.size() - 1);

		// if the ingredients are a quoted string, remove it
		if (b.contains("\"")) {
			ingredients = b.split("\"")[1].replace("\"","");
		}
		else {
			ingredients = b;
		}
	}

}
