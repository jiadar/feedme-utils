package co.fabrichealth.utils.parsers;

import java.util.Date;
import java.util.List;
import co.fabrichealth.utils.Nutrient;

/**
 * A parser interface to extract nutrition data by implementing parsers. 
 *
 * This is called by the Ingredient when attempting to parse a file detected as this type. 
 * Then the getter methods of this parser ae used to set the ingredient instance values. 
 *
 * To make a new parser, implement this class and return the values obtained from parsing
 * by way of the public methods. To get started, you could copy the StdParser class and
 * modify it. After making a new class, include a test for the new parser in Ingrident and 
 * add a parser name to the DataFormat enum. 
 *
 */
public interface UsdaParser {

	public String source();
	public Date collection_date();
	public String ndb();
	public String upc();
	public String name();
	public String group();
	public String cn();
	public List<Nutrient> nutrients();
	public String ingredients();

}
