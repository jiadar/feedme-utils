package co.fabrichealth.utils.parsers;

import java.util.Date;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import co.fabrichealth.utils.Nutrient;

/**
 * A parser to extract nutrition data from standard items of the USDA database.
 *
 * This is called by the Ingredient when attempting to parse a file detected as this type. 
 * Then the getter methods of this parser ae used to set the ingredient instance values. 
 *
 * NOTE: To make a new parser, copy this class and modify the constants and methods as desired. 
 * Then, hook it into Ingredient class with a detection method and a DataFormat
 *
 */
public class StdParser implements UsdaParser {

	// These constants determine how the matching methods work. For small changes, you may be able
	// to just change these values and have a new parser. 

	// TODO: Move these into a class that can be passed into generic methods for matching

	// The pattern which matches the nutrient headers. We will not parse these lines into nutrients. 
	private static final Pattern NUTRIENT_REGEX =
		Pattern.compile("Proximates|Minerals|Vitamins|Lipids|Amino Acids|Flavonoids|Isoflavones|Other");

	// The pattern to match the name, upc, and ndb fields 
	private static final Pattern NAME_REGEX = 
		Pattern.compile("\"Nutrient data for: (\\d+), (.*)");

	// The following line numbers start at 1 for the first line

	private static final int GROUP_LINE = 5;    	// The line number containing the group
	private static final int CN_LINE = 6;        // common name 
	private static final int NAME_LINE = 4;      // Line to match against NAME_REGEX
	private static final int NDB_FIELD = 1;      // Field in the NAME_REGEX that has the ndb
	private static final int NAME_FIELD = 2;     // Field in the NAME_REGEX that has the name
	private static final int UPC_FIELD = 0;      // Field in the NAME_REGEX that has the upc
	private static final int AMT_FIELD = 2;      // Field in the nutrient CSV that has the amount per 100g

	/* Instance Variables */

	private String source;
	private Date collection_date;
	private String ndb;
	private String upc;
	private String name;
	private String group;
	private String common_name;
	private List<Nutrient> nutrients;
	private String ingredients;

	/* Constructors */
	
	public StdParser() {
		source = "unknown";
		collection_date = new Date();
		ndb = "";
		upc = "";
		name = "";
		group = "";
		common_name = "";
		nutrients = new ArrayList<Nutrient>();
		ingredients = "";
	}

	public StdParser(final List<String> data) {
		this();
		parseHeaders(data);
		parseNutrients(data);
		parseIngredients(data);
	}	

	// TODO: Make these parser classes generic and take a class of vars for matching

	/**
	 * Parses nutrients from a list of strings as formatted in the scrape
	 * data file. Populates the nutrients in a list. 
	 *
	 * The scraped data file contains nutrient headers and nutrients below those headers
	 * starting on line 7 and continuing through the end of the file, minus the last 2
	 * lines which are a string of ingredients.
	 *
	 * @param data  List of strings containing data from the scraped file
	 * @return  Populated instance variables for nutrients
	 */	
	private void parseNutrients(final List<String> data) {

		// Create an iterator with the subset of the list that contains nutrient values

		Iterator<String> dataIterator = data.subList(9, data.size() - 2).iterator();

		// Loop through the iterator adding nutrients that don't match the regex defining
		// nutrient types. When we find a line that doesn't match this, add a new
		// nutrient to the list with the constructor, which parses the CSV string

		while (dataIterator.hasNext()) {
			String line = dataIterator.next();
			Matcher m = NUTRIENT_REGEX.matcher(line);

			// start bad code - needed to detect 2 non-standard formats within this type
			// TODO: fix to have a list of patterns that we would match to stop the nutrient population

			if (line.contains("Sources of Data")) {
				break;
			}

			if (Pattern.compile("^\\d+,").matcher(line).find()) {
				break;
			}

			// end of bad code
			
			if (!m.find()) {
				nutrients.add(new Nutrient(line, AMT_FIELD));
			}
		}
	}

	/** 
	 * Parses the headers from a list of strings as formated in the scrape
	 *
	 * The scraped data contains headers in the first few lines. Here we extract those headers 
	 * into the instance variables. 
	 *
	 * @param data  List of strings containing the data from the scraped file
	 * @return  Populated instance variables for headers
	 */
	private void parseHeaders(final List<String> data) {
		
		source = data.get(0).substring(8);

		Matcher m = NAME_REGEX.matcher(data.get(NAME_LINE - 1));
		if (m.find()) {
			ndb = NDB_FIELD > 0 ? m.group(NDB_FIELD) : "";
			name = NAME_FIELD > 0 ? m.group(NAME_FIELD).replace("\"","") : "";
			upc = UPC_FIELD > 0 ? m.group(UPC_FIELD) : "";
		}
		group = data.get(4).replace("Food Group:  ", "");
		common_name = data.get(4).replace("Common Name:", "");
	}

	/**
	 * Parses the ingredients from a list of strings formatted in the scrape
	 *
	 * The scraped data has the last line containing ingredients. Here we extract the ingredients
	 * into the instance variables
	 *
	 * @param data  List of strings containing the data from the scraped file
	 * @return  Populated instance variables for ingredients 
	 */
	private void parseIngredients(final List<String> data) {
		// There are no ingredients for this parser type
		ingredients = "";
	}

	public String source() {
		return source;
	}

	public Date collection_date() {
		return collection_date;
	}

	public String ndb() {
		return ndb;
	}

	public String upc() {
		return upc;
	}

	public String name() {
		return name;
	}

	public String group() {
		return group;
	}

	public String cn() {
		return common_name;
	}

	public List<Nutrient> nutrients() {
		return nutrients;
	}

	public String ingredients() {
		return ingredients;
	}
	
}
