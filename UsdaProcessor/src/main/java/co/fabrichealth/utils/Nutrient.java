package co.fabrichealth.utils;
import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;

/** 
 * Represents a single nutrient.
 *
 * This class is used by Ingredient to build up the list of nutrients. 
 * 
 */
public class Nutrient {

	// TODO: implement category for nutrients. Also it is a bad name. Rename it
	// Currently the category is not detected and unused 
	public enum Category { PROXIMATE, MINERAL, VITAMIN, LIPID, AMINO_ACID, OTHER, UNCATEGORIZED, NULL }

	private Category category;   // Category of nutrient
	private String name;         // Name of nutrient 
	private String unit;         // Unit of measurement (mg, g)
	private double amount;       // amount of nutrient per 100g

	/* Constructors */
	
	/* Empty constructor */
	public Nutrient() {
		category = null;
		name = "";
		unit = "";
		amount = 0.0;
	}

	/**
	 * Parse a nutrient given a CSV String. 
	 *
	 * It is assumed that the amount per 100g is on column 5 of the CSV
	 *
	 * @param line A string with a CSV nutrient 
	 */ 
	public Nutrient(String line) {
		parseNutrient(line, Category.UNCATEGORIZED, 5);
	}

	/**
	 * Parse a nutrient given a CSV String into a category.  
	 *
	 * It is assumed that the amount per 100g is on column 5 of the CSV
	 * Currently category functionality is limited. 
	 *
	 * @param line A string with a CSV nutrient 
	 * @param category The category of the nutrient 
	 */ 
	public Nutrient(String line, Category category) {
		parseNutrient(line, category, 5);
	}

	/**
	 * Parse a nutrient given a CSV String with the amount at a particular column. 
	 *
	 * Some parsers require the setting of the column for the nutrient amount. This 
	 * constructor allows setting the column. 
	 *
	 * @param line A string with a CSV nutrient 
	 * @param field An integer representing the field that contains the amount
	 */ 
	public Nutrient(String line, int field) {
		parseNutrient(line, Category.UNCATEGORIZED, field);
	}

	/**
	 * Parse a nutrient given a CSV String, a category, and the amount column. 
	 *
	 * This method allows a nutrient to be parsed or re-parsed with all functionality. 
	 *
	 * @param line A string with a CSV nutrient 
	 * @param category A Category of nutrient 
	 * @param field The field that contains the amount
	 */
	public void parseNutrient(String line, Category category, int field) {
		String[] fields = split_csv(line);
		if (fields.length >= field) {
			this.category = category;
			this.name = fields[0].replace("\"","");
			this.unit = fields[1];
			this.amount = Double.parseDouble(fields[field]);
			// This will write all the nutrients to a file in order to create a database column schema
			// appendToFile("/Users/ross/work/feedme/java/UsdaProcessor/nutrients.txt");
		}
		else {  // We are attempting to add a blank line as a nutrient, so behave well
			this.category = Category.NULL;
			this.name = "";
			this.unit = "";
			this.amount = 0.0;
		}
	}

	/**
	 * Appends a nutrient name to a file provided as a string
	 *
	 * This method allows one to build a list of allowable nutrients
	 *
	 * @param filename The name of the file you want to append to
	 */ 
	public void appendToFile(String filename) {
		try {
			File file =new File(filename);

			if (! file.exists()){
				file.createNewFile();
			}
			
			FileWriter fw = new FileWriter(file,true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(this.name + "\n");
			bw.close();
      }
		catch(IOException e){
			System.out.println("Exception occurred:");
			e.printStackTrace();
       
		}
	}

	/**
	 * Returns a string representation of a nutrient 
	 *
	 * @return str String representation of nutrient
	 */ 
	public String toString() {
		String rval = "";
		rval += this.name + " = " + this.amount + "\n";
		return rval;
	}
	
	/* Getters */
	
	public Category category() {
		return category;
	}

	public String name() {
		return name;
	}

	public String unit() {
		return unit;
	}

	public double amount() {
		return amount;
	}
	
	/** 
	 * Split a CSV intelligently 
	 * @param line The line to be split
	 * @return split line array
	 */ 
	private String[] split_csv(String line) {
		String otherThanQuote = " [^\"] ";
		String quotedString = String.format(" \" %s* \" ", otherThanQuote);
		String regex = String.format("(?x) "+ // enable comments, ignore white spaces
		                             ",                         "+ // match a comma
		                             "(?=                       "+ // start positive look ahead
		                             "  (?:                     "+ //   start non-capturing group 1
		                             "    %s*                   "+ //     match 'otherThanQuote' zero or more times
		                             "    %s                    "+ //     match 'quotedString'
		                             "  )*                      "+ //   end group 1 and repeat it zero or more times
		                             "  %s*                     "+ //   match 'otherThanQuote'
		                             "  $                       "+ // match the end of the string
		                             ")                         ", // stop positive look ahead
		                             otherThanQuote, quotedString, otherThanQuote);

		return line.split(regex, -1);
	}

}

